# Generated from diff_dirs-0.1.2.gem by gem2rpm -*- rpm-spec -*-
%global gem_name diff_dirs

Name: rubygem-%{gem_name}
Version: 0.1.2
Release: 1%{?dist}
Summary: Ruby helper to diff two directories
License: MIT
URL: http://github.com/cmer/diff_dirs
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(mocha)
BuildRequires: rubygem(shoulda-context)
BuildRequires: rubygem(test-unit)
BuildArch: noarch

%description
Ruby helper to diff two directories.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
%setup -q -n  %{gem_name}-%{version}

%build
# Create the gem as gem install only works on a gem file
gem build ../%{gem_name}-%{version}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/



%check
pushd .%{gem_instdir}
# Better to override original test_helper.rb, since it needs too many changes.
cat > test/test_helper.rb << \EOF
require 'test/unit'
require 'shoulda/context'
require 'mocha/test_unit'
EOF

ruby -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.rdoc
%{gem_instdir}/Rakefile
%doc %{gem_instdir}/VERSION
%{gem_instdir}/test

%changelog
* Fri Sep 08 2017 Vít Ondruch <vondruch@redhat.com> - 0.1.2-1
- Initial package
